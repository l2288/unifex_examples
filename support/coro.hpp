#pragma once

#include <experimental/coroutine>

#include <future>

//////////////////////////////////////////////////////////////////////

template <typename T>
struct StdFutureCoroPromise {
  std::promise<T> promise_;

  auto get_return_object() {
    return promise_.get_future();
  }

  std::experimental::suspend_never initial_suspend() noexcept {
    return {};
  }

  std::experimental::suspend_never final_suspend() noexcept {
    return {};
  }

  void set_exception(std::exception_ptr e) {
    promise_.set_exception(std::move(e));
  }

  void unhandled_exception() {
    promise_.set_exception(std::current_exception());
  }

  void return_value(T value) {
    promise_.set_value(std::move(value));
  }
};

template <typename R, typename... Args>
struct std::experimental::coroutine_traits<std::future<R>, Args...> {
  using promise_type = StdFutureCoroPromise<R>;
};

//////////////////////////////////////////////////////////////////////

template <>
struct StdFutureCoroPromise<void> {
  std::promise<void> promise_;

  auto get_return_object() {
    return promise_.get_future();
  }

  std::experimental::suspend_never initial_suspend() noexcept {
    return {};
  }

  std::experimental::suspend_never final_suspend() noexcept {
    return {};
  }

  void set_exception(std::exception_ptr e) {
    promise_.set_exception(std::move(e));
  }

  void unhandled_exception() {
    promise_.set_exception(std::current_exception());
  }

  void return_void() {
    promise_.set_value();
  }
};

template <typename... Args>
struct std::experimental::coroutine_traits<std::future<void>, Args...> {
  using promise_type = StdFutureCoroPromise<void>;
};
