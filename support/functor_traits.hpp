#pragma once

#include <type_traits>

namespace support {

template <typename F, typename R, typename A>
A Helper(R (F::*)(A));

template <typename F, typename R, typename A>
A Helper(R (F::*)(A) const);

template <typename F>
struct FunctorTraits {
  using ArgumentType = decltype(Helper(&F::operator()));
  using ReturnType = decltype(std::declval<F>()(std::declval<ArgumentType>()));
};

}  // namespace support
