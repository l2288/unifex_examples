# Senders / Receivers

## Examples

- [Submit](submit/main.cpp)
- [Connect](connect/main.cpp)

## References

- [`std::execution` Proposal](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p2300r4.html)
- [A Universal Async Abstraction for C++](https://cor3ntin.github.io/posts/executors/)
- [CppCon 2019 / A Unifying Abstraction for Async in C++”](https://www.youtube.com/watch?v=tF-Nz4aRWAM)
- [libunifex](https://github.com/facebookexperimental/libunifex)
