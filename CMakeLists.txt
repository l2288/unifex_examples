cmake_minimum_required(VERSION 3.17)
project(unified_executors)

set(CMAKE_CXX_STANDARD 20)

add_compile_options(-fsanitize=address)
add_link_options(-fsanitize=address)

add_subdirectory(submit)
add_subdirectory(connect)

add_subdirectory(live/submit)
add_subdirectory(live/connect)
