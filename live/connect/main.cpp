#include <iostream>

#include <experimental/coroutine>

#include <type_traits>
#include <thread>
#include <chrono>
#include <optional>
#include <functional>
#include <queue>

#include "../../support/coro.hpp"
#include "../../support/unit.hpp"
#include "../../support/functor_traits.hpp"

using namespace std::chrono_literals;

using std::experimental::coroutine_handle;

//////////////////////////////////////////////////////////////////////

// Concepts

// ~ Callback
template <typename R, typename T>
concept ReceiverOf = requires (R r, T value) {
  r.SetValue(value);
  r.SetError(std::exception_ptr{});  // Error handling
  r.SetDone();  // Cancellation
};

namespace detail {

template <typename T>
struct DummyReceiverOf {
  void SetValue(T) {}
  void SetError(std::exception_ptr) {}
  void SetDone() {}
};

}  // namespace detail

template <typename S>
concept Sender = requires (S s) {
  typename S::ValueType;
  s.Submit(detail::DummyReceiverOf<typename S::ValueType>{});
};

//////////////////////////////////////////////////////////////////////

// Receiver

namespace detail {

template<typename F>
struct FunctorReceiver {
  using T = typename support::FunctorTraits<F>::ArgumentType;

  FunctorReceiver(F f) : functor(f) {
  }

  void SetValue(T value) {
    functor(value);
  }

  // Error handling
  void SetError(std::exception_ptr) {
    std::abort();  // Not implemented
  }

  // Cancellation
  void SetDone() {
    std::abort();  // Not implemented
  }

  F functor;
};

}  // namespace detail

template <typename F>
auto AsReceiver(F f) {
  return detail::FunctorReceiver<F>(f);
}

//////////////////////////////////////////////////////////////////////

// `ThreadPool`

class ThreadPool {
 public:
  using Task = std::function<void()>;

  struct Sender {
    using ValueType = Unit;

    ThreadPool& tp;

    template <typename R>
    void Submit(R r) {
      tp.Submit([r]() mutable {
        r.SetValue(Unit{});
      });
    }
  };

  struct Scheduler {
    ThreadPool& tp;

    auto Schedule() {
      return Sender{tp};
    }
  };

  Scheduler GetScheduler() {
    return Scheduler{*this};
  }

 private:
  void Submit(Task task) {
    std::cout << "Run task" << std::endl;
    task();
  }
};

//////////////////////////////////////////////////////////////////////

// Generic operations

template <typename S, typename R>
void Submit(S& sender, R receiver) {
  sender.Submit(receiver);
}

//////////////////////////////////////////////////////////////////////

// Coroutine integration

// Receiver ~ Coroutine
// Sender ~ Awaiter
// Submit ~ await_suspend

namespace detail {

template <Sender S>
struct SenderAwaiter {
  S sender;

  using T = typename S::ValueType;

  std::optional<T> value;

  bool await_ready() {
    return false;
  }

  void await_suspend(std::experimental::coroutine_handle<> h) {
    sender.Submit(AsReceiver([h, this](T v) mutable {
      value.emplace(v);
      h.resume();
    }));
  }

  T await_resume() {
    return *value;
  }
};

}

template <Sender S>
auto operator co_await(S sender) {
  return detail::SenderAwaiter<S>{sender};
}

template <typename Scheduler>
std::future<void> Coro(Scheduler scheduler) {
  co_await scheduler.Schedule();
  std::cout << "Hi from coroutine" << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  {
    ThreadPool tp;
    auto tp_scheduler = tp.GetScheduler();

    auto sender = tp_scheduler.Schedule();
    auto receiver = AsReceiver([](Unit) {
      std::cout << "Hi from automatic operation" << std::endl;
    });

    Submit(sender, receiver);
  }

  {
    ThreadPool tp;
    auto tp_scheduler = tp.GetScheduler();

    Coro(tp_scheduler);
  }

  return 0;
}
