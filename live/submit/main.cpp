#include <iostream>
#include <functional>
#include <type_traits>
#include <thread>
#include <chrono>
#include <optional>
#include <deque>

#include <experimental/coroutine>

#include "../../support/coro.hpp"

using namespace std::chrono_literals;

using std::experimental::coroutine_handle;

//////////////////////////////////////////////////////////////////////

// Meta-programming helpers

namespace support {

template <typename F, typename R, typename A>
A Helper(R (F::*)(A));

template <typename F, typename R, typename A>
A Helper(R (F::*)(A) const);

template <typename F>
struct FunctorTraits {
  using ArgumentType = decltype(Helper(&F::operator()));
  using ReturnType = decltype(std::declval<F>()(std::declval<ArgumentType>()));
};

}  // namespace support

//////////////////////////////////////////////////////////////////////

// Unit type

struct Unit {
};

//////////////////////////////////////////////////////////////////////

// Concepts

// ~ Callback
template <typename R, typename T>
concept ReceiverOf = requires (R r, T value) {
  r.SetValue(value);
  r.SetError(std::exception_ptr{});  // Error handling
  r.SetDone();  // Cancellation
};

namespace detail {

template <typename T>
struct DummyReceiverOf {
  void SetValue(T) {}
  void SetError(std::exception_ptr) {}
  void SetDone() {}
};

}  // namespace detail

template <typename S>
concept Sender = requires (S s) {
  typename S::ValueType;
  s.Submit(detail::DummyReceiverOf<typename S::ValueType>{});
};

//////////////////////////////////////////////////////////////////////

// AsReceiver

namespace detail {

template<typename F>
struct FunctorReceiver {
  using T = typename support::FunctorTraits<F>::ArgumentType;

  FunctorReceiver(F f) : functor(f) {
  }

  void SetValue(T value) {
    functor(value);
  }

  void SetError(std::exception_ptr) {
    std::abort();  // Not supported
  }

  void SetDone() {
    std::abort();  // Not supported
  }

  F functor;
};

}  // namespace detail

template <typename F>
auto AsReceiver(F f) {
  return detail::FunctorReceiver<F>(f);
}

//////////////////////////////////////////////////////////////////////

// `Just` sender

namespace detail {

template <typename T>
struct JustSender {
  T value;

  using ValueType = T;

  template <typename R>
  void Submit(R receiver) {
    receiver.SetValue(value);
  }
};

}

template <typename T>
auto Just(T value) {
  return detail::JustSender<T>{value};
}

//////////////////////////////////////////////////////////////////////

// `NewThread` sender

namespace detail {

struct NewThreadSender {
  using ValueType = Unit;

  template <typename R>
  void Submit(R r) {
    std::thread([r]() mutable {
      r.SetValue(Unit{});
    }).detach();
  }
};

}

auto NewThread() {
  return detail::NewThreadSender{};
}

//////////////////////////////////////////////////////////////////////

// `ThreadPool` sender

class ThreadPool {
 public:
  using Task = std::function<void()>;

  struct Sender {
    using ValueType = Unit;

    ThreadPool& tp;

    template <typename R>
    void Submit(R r) {
      tp.Submit([r]() mutable {
        r.SetValue(Unit{});
      });
    }
  };

  struct Scheduler {
    ThreadPool& tp;

    auto Schedule() {
      return Sender{tp};
    }
  };

  Scheduler GetScheduler() {
    return Scheduler{*this};
  }

 private:
  void Submit(Task task) {
    std::cout << "Run task" << std::endl;
    task();
  }
};

//////////////////////////////////////////////////////////////////////

// Then combinator

//////////////////////////////////////////////////////////////////////

// Coroutine integration

//////////////////////////////////////////////////////////////////////

// Generic operations

//////////////////////////////////////////////////////////////////////

int main() {
  // Receiver

  {
    auto r = AsReceiver([](int value) {
      std::cout << "Received value " << value << std::endl;
    });
    r.SetValue(17);
  }

  return 0;
}
